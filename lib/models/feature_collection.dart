// To parse this JSON data, do
//
//     final featureCollection = featureCollectionFromMap(jsonString);

import 'dart:convert';

class FeatureCollection {
  FeatureCollection({
    this.type,
    this.version,
    this.features,
    this.attribution,
    this.licence,
    this.query,
    this.limit,
  });

  final String type;
  final String version;
  final List<Feature> features;
  final String attribution;
  final String licence;
  final String query;
  final int limit;

  FeatureCollection copyWith({
    String type,
    String version,
    List<Feature> features,
    String attribution,
    String licence,
    String query,
    int limit,
  }) =>
      FeatureCollection(
        type: type ?? this.type,
        version: version ?? this.version,
        features: features ?? this.features,
        attribution: attribution ?? this.attribution,
        licence: licence ?? this.licence,
        query: query ?? this.query,
        limit: limit ?? this.limit,
      );

  factory FeatureCollection.fromJson(String str) => FeatureCollection.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory FeatureCollection.fromMap(Map<String, dynamic> json) => FeatureCollection(
        type: json["type"],
        version: json["version"],
        features: List<Feature>.from(json["features"].map((x) => Feature.fromMap(x))),
        attribution: json["attribution"],
        licence: json["licence"],
        query: json["query"],
        limit: json["limit"],
      );

  Map<String, dynamic> toMap() => {
        "type": type,
        "version": version,
        "features": List<dynamic>.from(features.map((x) => x.toMap())),
        "attribution": attribution,
        "licence": licence,
        "query": query,
        "limit": limit,
      };
}

class Feature {
  Feature({
    this.type,
    this.geometry,
    this.properties,
  });

  final String type;
  final Geometry geometry;
  final Properties properties;

  Feature copyWith({
    String type,
    Geometry geometry,
    Properties properties,
  }) =>
      Feature(
        type: type ?? this.type,
        geometry: geometry ?? this.geometry,
        properties: properties ?? this.properties,
      );

  factory Feature.fromJson(String str) => Feature.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Feature.fromMap(Map<String, dynamic> json) => Feature(
        type: json["type"],
        geometry: Geometry.fromMap(json["geometry"]),
        properties: Properties.fromMap(json["properties"]),
      );

  Map<String, dynamic> toMap() => {
        "type": type,
        "geometry": geometry.toMap(),
        "properties": properties.toMap(),
      };
}

class Geometry {
  Geometry({
    this.type,
    this.coordinates,
  });

  final String type;
  final List<double> coordinates;

  Geometry copyWith({
    String type,
    List<double> coordinates,
  }) =>
      Geometry(
        type: type ?? this.type,
        coordinates: coordinates ?? this.coordinates,
      );

  factory Geometry.fromJson(String str) => Geometry.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Geometry.fromMap(Map<String, dynamic> json) => Geometry(
        type: json["type"],
        coordinates: List<double>.from(json["coordinates"].map((x) => x.toDouble())),
      );

  Map<String, dynamic> toMap() => {
        "type": type,
        "coordinates": List<dynamic>.from(coordinates.map((x) => x)),
      };
}

class Properties {
  Properties({
    this.label,
    this.score,
    this.housenumber,
    this.id,
    this.type,
    this.name,
    this.postcode,
    this.citycode,
    this.x,
    this.y,
    this.city,
    this.context,
    this.importance,
    this.street,
  });

  final String label;
  final double score;
  final String housenumber;
  final String id;
  final String type;
  final String name;
  final String postcode;
  final String citycode;
  final double x;
  final double y;
  final String city;
  final String context;
  final double importance;
  final String street;

  Properties copyWith({
    String label,
    double score,
    String housenumber,
    String id,
    String type,
    String name,
    String postcode,
    String citycode,
    double x,
    double y,
    String city,
    String context,
    double importance,
    String street,
  }) =>
      Properties(
        label: label ?? this.label,
        score: score ?? this.score,
        housenumber: housenumber ?? this.housenumber,
        id: id ?? this.id,
        type: type ?? this.type,
        name: name ?? this.name,
        postcode: postcode ?? this.postcode,
        citycode: citycode ?? this.citycode,
        x: x ?? this.x,
        y: y ?? this.y,
        city: city ?? this.city,
        context: context ?? this.context,
        importance: importance ?? this.importance,
        street: street ?? this.street,
      );

  factory Properties.fromJson(String str) => Properties.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Properties.fromMap(Map<String, dynamic> json) => Properties(
        label: json["label"],
        score: json["score"].toDouble(),
        housenumber: json["housenumber"],
        id: json["id"],
        type: json["type"],
        name: json["name"],
        postcode: json["postcode"],
        citycode: json["citycode"],
        x: json["x"].toDouble(),
        y: json["y"].toDouble(),
        city: json["city"],
        context: json["context"],
        importance: json["importance"].toDouble(),
        street: json["street"],
      );

  Map<String, dynamic> toMap() => {
        "label": label,
        "score": score,
        "housenumber": housenumber,
        "id": id,
        "type": type,
        "name": name,
        "postcode": postcode,
        "citycode": citycode,
        "x": x,
        "y": y,
        "city": city,
        "context": context,
        "importance": importance,
        "street": street,
      };
}
