library fr_geo_gouv_api;

import 'dart:convert';
import 'package:fr_geo_gouv_api/api_client.dart';
import 'package:fr_geo_gouv_api/models/feature_collection.dart';
import 'package:http/http.dart';

enum SearchType { housenumber, street, locality, municipality }

extension on SearchType {
  String toShortString() {
    return this.toString().split('.')[1];
  }
}

/// Search addresse.
class Search {
  Search._internal();

  static final Search _instance = Search._internal();

  factory Search() {
    return _instance;
  }
  ApiClient apiClient = ApiClient();

  Future<Response> searchWithHttpInfo({String q, SearchType type, int limit, bool autocomplete}) async {
    // Object postBody = inlineObject3;

    // verify required params are set
    if (q == null) {
      throw ApiException(400, "Missing required param: inlineObject3");
    }

    // create path and map variables
    String path = "/search".replaceAll("{format}", "json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    Map<String, dynamic> params = {'q': q, 'type': type.toShortString(), 'limit': limit, 'autocomplete': autocomplete};
    params.forEach((name, value) => queryParams.add(QueryParam(name, value.toString())));

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["ApiKeyAuth"];

    if (nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      // if (hasFields) postBody = mp;
    } else {}

    var response = await apiClient.invokeAPI(path, 'GET', queryParams, null, headerParams, formParams, nullableContentType, authNames);
    return response;
  }

  Future<FeatureCollection> search({String q, SearchType type, int limit, bool autocomplete}) async {
    Response response = await searchWithHttpInfo(q: q, type: type, limit: limit, autocomplete: autocomplete);
    if (response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if (response.body != null) {
      print(response.body);
      return apiClient.deserialize(_decodeBodyBytes(response), 'FeatureCollection');
    } else {
      return null;
    }
  }
}

String _decodeBodyBytes(Response response) {
  var contentType = response.headers['content-type'];
  if (contentType != null && contentType.contains("application/json")) {
    return utf8.decode(response.bodyBytes);
  } else {
    return response.body;
  }
}

class ApiException implements Exception {
  int code = 0;
  String message;
  Exception innerException;
  StackTrace stackTrace;

  ApiException(this.code, this.message);

  ApiException.withInner(this.code, this.message, this.innerException, this.stackTrace);

  String toString() {
    if (message == null) return "ApiException";

    if (innerException == null) {
      return "ApiException $code: $message";
    }

    return "ApiException $code: $message (Inner exception: $innerException)\n\n" + stackTrace.toString();
  }
}
