import 'dart:convert';
import 'package:fr_geo_gouv_api/fr_geo_gouv_api.dart';
import 'package:fr_geo_gouv_api/models/feature_collection.dart';
import 'package:http/http.dart';

class QueryParam {
  String name;
  String value;

  QueryParam(this.name, this.value);
}

class ApiClient {
  String basePath;
  var client = Client();

  Map<String, String> _defaultHeaderMap = {};

  final _regList = RegExp(r'^List<(.*)>$');
  final _regMap = RegExp(r'^Map<String,(.*)>$');

  ApiClient({this.basePath = "https://api-adresse.data.gouv.fr/"}) {
    // Setup authentications (key: authentication name, value: authentication).
    // _authentications['ApiKeyAuth'] = ApiKeyAuth("header", "x-access-token");
  }

  void addDefaultHeader(String key, String value) {
    _defaultHeaderMap[key] = value;
  }

  dynamic _deserialize(dynamic value, String targetType) {
    try {
      switch (targetType) {
        case 'String':
          return '$value';
        case 'int':
          return value is int ? value : int.parse('$value');
        case 'bool':
          return value is bool ? value : '$value'.toLowerCase() == 'true';
        case 'double':
          return value is double ? value : double.parse('$value');
        case 'FeatureCollection':
          return FeatureCollection.fromMap(value);
        case 'Feature':
          return Feature.fromMap(value);
        case 'Geometry':
          return Geometry.fromMap(value);
        case 'Properties':
          return Properties.fromMap(value);
        default:
          {
            Match match;
            if (value is List && (match = _regList.firstMatch(targetType)) != null) {
              var newTargetType = match[1];
              return value.map((v) => _deserialize(v, newTargetType)).toList();
            } else if (value is Map && (match = _regMap.firstMatch(targetType)) != null) {
              var newTargetType = match[1];
              return Map.fromIterables(value.keys, value.values.map((v) => _deserialize(v, newTargetType)));
            }
          }
      }
    } on Exception catch (e, stack) {
      throw ApiException.withInner(500, 'Exception during deserialization.', e, stack);
    }
    throw ApiException(500, 'Could not find a suitable class for deserialization');
  }

  dynamic deserialize(String json, String targetType) {
    // Remove all spaces.  Necessary for reg expressions as well.
    targetType = targetType.replaceAll(' ', '');

    if (targetType == 'String') return json;

    var decodedJson = jsonDecode(json);
    return _deserialize(decodedJson, targetType);
  }

  String serialize(Object obj) {
    String serialized = '';
    if (obj == null) {
      serialized = '';
    } else {
      serialized = json.encode(obj);
    }
    return serialized;
  }

  // We don't use a Map<String, String> for queryParams.
  // If collectionFormat is 'multi' a key might appear multiple times.
  Future<Response> invokeAPI(String path, String method, List<QueryParam> queryParams, Object body, Map<String, String> headerParams,
      Map<String, String> formParams, String nullableContentType, List<String> authNames) async {
    // _updateParamsForAuth(authNames, queryParams, headerParams);

    var ps = queryParams.where((p) => p.value != null).map((p) => '${p.name}=${Uri.encodeQueryComponent(p.value)}');

    String queryString = ps.isNotEmpty ? '?' + ps.join('&') : '';

    String url = basePath + path + queryString;

    headerParams.addAll(_defaultHeaderMap);
    if (nullableContentType != null) {
      final contentType = nullableContentType;
      headerParams['Content-Type'] = contentType;
    }

    if (body is MultipartRequest) {
      var request = MultipartRequest(method, Uri.parse(url));
      request.fields.addAll(body.fields);
      request.files.addAll(body.files);
      request.headers.addAll(body.headers);
      request.headers.addAll(headerParams);
      var response = await client.send(request);
      return Response.fromStream(response);
    } else {
      var msgBody = nullableContentType == "application/x-www-form-urlencoded" ? formParams : serialize(body);
      final nullableHeaderParams = (headerParams.isEmpty) ? null : headerParams;
      switch (method) {
        case "POST":
          return client.post(Uri.parse(url), headers: nullableHeaderParams, body: msgBody);
        case "PUT":
          return client.put(Uri.parse(url), headers: nullableHeaderParams, body: msgBody);
        case "DELETE":
          return client.delete(Uri.parse(url), headers: nullableHeaderParams);
        case "PATCH":
          return client.patch(Uri.parse(url), headers: nullableHeaderParams, body: msgBody);
        case "HEAD":
          return client.head(Uri.parse(url), headers: nullableHeaderParams);
        default:
          return client.get(Uri.parse(url), headers: nullableHeaderParams);
      }
    }
  }
}
