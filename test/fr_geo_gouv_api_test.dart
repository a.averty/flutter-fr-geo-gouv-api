import 'package:flutter_test/flutter_test.dart';
import 'package:fr_geo_gouv_api/fr_geo_gouv_api.dart';
import 'package:fr_geo_gouv_api/models/feature_collection.dart';

void main() {
  test('Search addresse', () async {
    FeatureCollection fc = await Search().search(
      q: '1 bis rue fulton',
      type: SearchType.housenumber,
      limit: 5,
      autocomplete: true,
    );
    print(fc);
    print('\n======================================\n\n');
    FeatureCollection fc2 = await Search().search(
      q: 'rue fulton',
      type: SearchType.street,
      limit: 1,
      autocomplete: true,
    );
    print(fc2);
    print('\n======================================\n\n');
    FeatureCollection fc3 = await Search().search(
      q: 'Nantes',
      type: SearchType.locality,
      limit: 2,
      autocomplete: false,
    );
    print(fc3);
    print('\n======================================\n\n');
    FeatureCollection fc4 = await Search().search(
      q: 'Nantes',
      type: SearchType.municipality,
      limit: 2,
      autocomplete: false,
    );
    print(fc4);
    expect(fc, isInstanceOf<FeatureCollection>());
    expect(fc2, isInstanceOf<FeatureCollection>());
    expect(fc3, isInstanceOf<FeatureCollection>());
    expect(fc4, isInstanceOf<FeatureCollection>());
  });
}
